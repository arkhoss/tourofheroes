# Tourofheroes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## How to run this repo
Single container
```
docker run -d --restart=unless-stopped --log-driver json-file --net="host" --expose=4200/tcp -p 4200:4200 -v /home/centos/bitbucket/tourofheroes:/app --name tourofheroes_app_1 arkhoss/tourofheroes
```

## Run whole Project DB + API + Angular App
Run
```
  docker-compose up -d
```

Stop and Delete
```
  docker-compose down
```

## How to login to mySQL db
```
docker exec -ti tourofheroes_db_1 mysql -u root -p
```

## Author
David Caballero
